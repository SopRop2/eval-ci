var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');

gulp.task('js', function () {
    return gulp.src('src/*.html')
        .pipe(useref())
        .pipe(gulpIf('js/*.js', uglify()))
        .pipe(gulp.dest('dist'))
});

gulp.task('css', function(){
    return gulp.src('src/styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/styles'))
});
